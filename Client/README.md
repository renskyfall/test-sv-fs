TES-SV-BRI Client
--------------

Versi npm = 7.21.0

CoreUI-React = 3.0.0


Install app's dependencies
---------------
npm install

Install axios
---------------
npm install axios

Run Application
---------------
Run Terminal (pada folder ini) = npm start

Navigate to [http://localhost:3000](http://localhost:3000). The app will automatically reload if you change any of the source files.

HOW TO USE
----------

Halaman User Management
1. User List
Showing User List Data

2. Show Entries
Choose total data that gonna show in User List

3. Page
Pagination to User List Data

4. Button Create & Delete User
Button linked to Create & Delete User Page

Halaman Create & Delete User
1. Create User
Create Data User

2. Pop Up Delete User
Delete Data User By Id
