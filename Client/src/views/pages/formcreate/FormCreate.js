import React from 'react';
import axios from 'axios';
import {
  CButton,
  CCard,
  CCardBody,
  CContainer,
  CFormGroup,
  CLabel,
  CInput,
  CAlert
} from '@coreui/react';

export default class UserCreate extends React.Component {
  constructor() {
    super()
    this.state = {
        username: '',
        password: '',
        name: '',
        idUser: '',
        createSuccess: null,
        deleteSuccess: null,
        isShowPopup: false
    }
}


  _handleSubmit(){
    
    var user = {
      name: this.state.name,
      username: this.state.username,
      password: this.state.password
    };
    console.log(user)

    axios.post('http://localhost:8089/user', user)
      .then(res => {
        console.log(res);
        if (res.status === 200) {
          this.setState({ createSuccess: 1 })
        } else {
          this.setState({ createSuccess: 0 })
        }

        
      })

      
      setTimeout(
        function () {
            this.setState({ createSuccess: null })
        }
            .bind(this), 2000
      );
  }

  _alertCreate() {
    if (this.state.createSuccess === 1) {
        return (
            <CAlert color="info">Create user success!</CAlert>
        )
    } else if (this.state.createSuccess === 0) {
        return (
            <CAlert color="danger">Create user error!</CAlert>
        )
    } else {
        return null
    }
  }

  _deleteUserAction(){
    
    axios.delete(`http://localhost:8089/user/${this.state.idUser}`).then(res => {
      console.log(res)
      if (res.status === 200) {
          this.setState({ deleteSuccess: 1 })
      } else {
          this.setState({ deleteSuccess: 0 })
      }
    })
    setTimeout(
        function () {
            this.setState({ deleteSuccess: null })
        }
            .bind(this), 2000
    );
  }

  _alertDelete() {
    if (this.state.deleteSuccess === 1) {
        return (
            <CAlert color="info">Delete user success!</CAlert>
        )
    } else if (this.state.deleteSuccess === 0) {
        return (
            <CAlert color="danger">Delete user error!</CAlert>
        )
    } else {
        return null
    }
  }

  _popupDeleteUser() {
    return (
        <CCardBody style={{
            backgroundColor: '#ffffff',
            position: 'absolute',
            left: '50%',
            width: '300px',
            height: '200px',
            transform: `translate(${-50}%, ${0}%)`,
            borderRadius: '8px',
            boxShadow: '0px 00px 20px rgba(0, 0, 0, 0.20)'
        }}>
            <h1 style={{ position: 'absolute', fontSize: '18px' }}>Delete User by Id</h1>
            <CButton style={{ marginLeft: '240px' }}
                variant="outline"
                color={"danger"}
                size="sm"
                onClick={() => this.setState({ isShowPopup: false, idUser: '' })}
            >x</CButton>
            <CFormGroup style={{ marginTop: '35px' }}>
                <CInput
                    type="number"
                    placeholder="Id User..."
                    onChange={(e) => { this.setState({ idUser: e.target.value }) }}
                />
            </CFormGroup>
            <CButton style={{ position: 'absolute', left: '50%', transform: `translate(${-50}%, ${0}%)`, marginTop: '10px' }}
                onClick={() => { this._deleteUserAction() }}
                color="danger"
                disabled={this.state.idUser.length === '' ? true : false}>
                Delete
            </CButton>
        </CCardBody>
    )
  }

  render() {
    return (
      <div className="c-app c-default-layout flex-row align-items-center" >
                <CContainer>
                    {this._alertCreate()}
                    {this._alertDelete()}
                    <CCard>
                        <CCardBody>
                            <h1 style={{ marginBottom: '25px' }}>Create User</h1>
                            <CCardBody style={{ backgroundColor: '#ebedef' }}>
                                <CFormGroup>
                                    <CLabel htmlFor="nf-email" style={{ fontSize: '16px', fontWeight: 500 }}>Username</CLabel>
                                    <CInput
                                        type="text"
                                        placeholder="Enter Username..."
                                        onChange={(e) => { this.setState({ username: e.target.value }) }}
                                    />
                                    {this.state.username.length > 0 && this.state.username.length < 3 ?
                                        <p style={{ color: '#e55353' }}>Username minimal 3 karakter!</p>
                                        : null
                                    }
                                </CFormGroup>

                                <CFormGroup>
                                    <CLabel htmlFor="nf-email" style={{ fontSize: '16px', fontWeight: 500 }}>Password</CLabel>
                                    <CInput
                                        type="password"
                                        placeholder="Enter Password..."
                                        onChange={(e) => { this.setState({ password: e.target.value }) }}
                                    />
                                    {this.state.password.length > 0 && this.state.password.length < 7 ?
                                        <p style={{ color: '#e55353' }}>Password minimal 7 karakter!</p>
                                        : null
                                    }
                                </CFormGroup>

                                <CFormGroup>
                                    <CLabel htmlFor="nf-email" style={{ fontSize: '16px', fontWeight: 500 }}>Name</CLabel>
                                    <CInput
                                        type="text"
                                        placeholder="Enter Name..."
                                        onChange={(e) => { this.setState({ name: e.target.value }) }}
                                    />
                                    {this.state.name.length > 0 && this.state.name.length < 3 ?
                                        <p style={{ color: '#e55353' }}>Name minimal 3 karakter!</p>
                                        : null
                                    }
                                </CFormGroup>

                                <CButton style={{ marginTop: '20px' }}
                                    variant="outline"
                                    color={"primary"}
                                    onClick={() => this._handleSubmit()}>
                                    Submit
                                </CButton>
                            </CCardBody>

                            <h1 style={{ margin: '20px 0' }}>Delete User</h1>
                            <CCardBody style={{ backgroundColor: '#ebedef' }}>
                                <CButton
                                    variant="outline"
                                    color={"danger"}
                                    onClick={() => this.setState({ isShowPopup: true })}>
                                    Show Popup Delete
                                </CButton>
                            </CCardBody>
                            
                        </CCardBody>
                    </CCard>
                </CContainer>
                {this.state.isShowPopup ? this._popupDeleteUser() : null}
            </div>
    )
  }
}