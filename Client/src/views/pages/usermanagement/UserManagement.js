import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CDropdown,
  CDropdownToggle,
  CDropdownMenu,
  CDropdownItem,
  CButton} from "@coreui/react";
import axios from 'axios';
import { Link } from 'react-router-dom'
import { useEffect, useState } from 'react';
import './UserManagement.css'

const LOCALSTYLES = {
  textFooter: {
      color: '#ebedef',
      fontSize: '16px',
      margin: '0 10px'
  },
  cardFooter1: {
      backgroundColor: '#4f5d73',
      width: '100%',
      marginTop: '5px',
      display: 'flex',
      justifyContent: 'flex-start'
  },
  cardFooter2: {
      backgroundColor: '#4f5d73',
      width: '100%',
      marginTop: '5px',
      display: 'flex',
      justifyContent: 'flex-end'
  }
}

const dataUser = ["id", "name", "username", "password"]

export default function UserManagement() {
  
  const [listUsers, setListUsers] = useState(null)
  const [entries, setEntries] = useState(5)
  const [page, setPage] = useState(1)

  useEffect(() => {
    var offset = entries * (page - 1);
    axios.get(`http://localhost:8089/users/${entries}/${offset}`).then(res => {
        console.log(res)
        setListUsers(res.data)
        console.log(listUsers)
    })
}, [entries, page])

useEffect(() => {
    setPage(1)
}, [entries])

  return(
      <CRow>
    <CCol>
      <CCard>
        <CCardHeader>
          <h1>User List</h1>
        </CCardHeader>
        <CCardBody>
        <CDataTable
          items={listUsers}
          fields={dataUser}
          hover
          striped
          itemsPerPage={entries}
          activePage={1}
        />
        </CCardBody>
        <CCardBody style={LOCALSTYLES.cardFooter1}>
            <p style={LOCALSTYLES.textFooter}>Show</p>
            <CDropdown>
                <CDropdownToggle color="secondary" size="sm">
                    {entries}
                </CDropdownToggle>
                <CDropdownMenu>
                    <CDropdownItem onClick={() => setEntries(5)} >5</CDropdownItem>
                    <CDropdownItem onClick={() => setEntries(10)}>10</CDropdownItem>
                    <CDropdownItem onClick={() => setEntries(20)}>20</CDropdownItem>
                    <CDropdownItem onClick={() => setEntries(50)}>50</CDropdownItem>
                </CDropdownMenu>
            </CDropdown>
            <p style={LOCALSTYLES.textFooter}>Entries</p>
        </CCardBody>

        <CCardBody style={LOCALSTYLES.cardFooter2}>
            <p style={LOCALSTYLES.textFooter}>Page:</p>
            <CButton
                color="secondary"
                size="sm"
                onClick={() => { page > 1 ? setPage(page - 1) : setPage(page) }}
            >
                {'<'}
            </CButton>
            <p style={LOCALSTYLES.textFooter}>{page}</p>
            <CButton
                color="secondary"
                size="sm"
                style={{ marginRight: '10px' }}
                onClick={() => setPage(page + 1)}
            >
                {'>'}
            </CButton>
        </CCardBody>
        <CCardBody>
          <Link to="/pages/createUser" style={{ textDecoration: 'none' }}>
            <CRow className="justify-content-end" >
              <CButton type="button" class="btn btn-info">
                Create & Delete User
              </CButton>
            </CRow>
          </Link>
        </CCardBody>
      </CCard>
    </CCol>
  </CRow>
  )
}
