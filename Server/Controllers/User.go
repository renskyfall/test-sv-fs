//Controllers/User.go
package Controllers

import (
	"fmt"
	"net/http"
	"strconv"
	"user-management/Models"
	"user-management/Services"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

//HashPassword
func HashPassword(password string) (string, error) {
    bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
    return string(bytes), err
}

//CheckPasswordHash
func CheckPasswordHash(password, hash string) bool {
    err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
    return err == nil
}

//ValidateStringLength
func ValidateStringLength(word string) int{
	alo := 0

	for range word {
		alo++
	}

	return alo
}


//GetUsers ... Get all users
func GetUsers(c *gin.Context) {
	var user []Models.User
	limit, _ := strconv.Atoi(c.Params.ByName("limit"))
	offset, _ := strconv.Atoi(c.Params.ByName("offset"))
	err := Services.GetAllUsers(&user,limit, offset)

	if err != nil {
		c.JSON(http.StatusBadRequest, err)
	} else {
		c.JSON(http.StatusOK, user)
	}
}


//CreateUser ... Create User
func CreateUser(c *gin.Context) {
	var user Models.User
	c.BindJSON(&user)

	usernameLength := ValidateStringLength(string(*user.Username))
	if usernameLength < 3 {
		c.JSON(http.StatusBadRequest, gin.H{"Message": "Failed save Data, Username must be 3 characters or more"})
		return 
	}

	passwordLength := ValidateStringLength(string(*user.Password))
	if passwordLength < 7 {
		c.JSON(http.StatusBadRequest, gin.H{"Message": "Failed save Data, Password must be 7 characters or more"})
		return 
	}

	nameLength := ValidateStringLength(user.Name)
	if nameLength < 3 {
		c.JSON(http.StatusBadRequest, gin.H{"Message": "Failed save Data, Name must be 3 characters or more"})
		return 
	}

	hashedPassword, _ := HashPassword(string(*user.Password))
	user.Password = &hashedPassword

	err := Services.CreateUser(&user)

	if err != nil {
	fmt.Println(err.Error())
		c.JSON(http.StatusBadRequest, err)
	} else {
		c.JSON(http.StatusOK, user)
	}
}

//GetUserByID ... Get the user by id
func GetUserByID(c *gin.Context) {
	id := c.Params.ByName("id")
	var user Models.User
	err := Services.GetUserByID(&user, id)
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
	} else {
		c.JSON(http.StatusOK, user)
	}
}

//UpdateUser ... Update the user information
func UpdateUser(c *gin.Context) {
	var user Models.User
	id := c.Params.ByName("id")
	err := Services.GetUserByID(&user, id)
	if err != nil {
		c.JSON(http.StatusNotFound, err)
	}
	c.BindJSON(&user)

	usernameLength := ValidateStringLength(string(*user.Username))
	if usernameLength < 3 {
		c.JSON(http.StatusBadRequest, gin.H{"Message": "Failed save Data, Username must be 3 characters or more"})
		return 
	}

	passwordLength := ValidateStringLength(string(*user.Password))
	if passwordLength < 7 {
		c.JSON(http.StatusBadRequest, gin.H{"Message": "Failed save Data, Password must be 7 characters or more"})
		return 
	}

	nameLength := ValidateStringLength(user.Name)
	if nameLength < 3 {
		c.JSON(http.StatusBadRequest, gin.H{"Message": "Failed save Data, Name must be 3 characters or more"})
		return 
	}

	hashedPassword, _ := HashPassword(string(*user.Password))
	user.Password = &hashedPassword

	err = Services.UpdateUser(&user)
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
	} else {
		c.JSON(http.StatusOK, user)
	}
}

//DeleteUser ... Delete the user
func DeleteUser(c *gin.Context) {
	var user Models.User
	id := c.Params.ByName("id")
	err := Services.DeleteUser(&user, id)
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
	} else {
		c.JSON(http.StatusOK, gin.H{"Message" : "id" + id + "is deleted"})
	}
}