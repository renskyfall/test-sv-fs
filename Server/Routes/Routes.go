//Routes/Routes.go
package Routes

import (
	"user-management/Controllers"
	"github.com/gin-gonic/gin"
	"github.com/gin-contrib/cors"
)

//SetupRouter ... Configure routes
func SetupRouter() *gin.Engine {
	r := gin.Default()
	// config := cors.DefaultConfig()
  	// config.AllowOrigins = []string{"http://localhost:8089", "http://localhost:3000"}
	// // config.AllowOrigins = []string{"http://localhost:3000"}
	// r.Use(cors.New(cors.Config{
	// 	AllowOrigins:     []string{"*"},
	// 	AllowMethods:     []string{"PUT", "PATCH", "GET", "POST", "OPTION"},
	// 	AllowHeaders:     []string{"Origin"},
	// 	ExposeHeaders:    []string{"Content-Length"},
	// 	AllowCredentials: true,
	// }))
	r.Use(cors.Default())
	
	r.GET("users/:limit/:offset", Controllers.GetUsers)
	r.POST("user", Controllers.CreateUser)
	r.GET("user/:id", Controllers.GetUserByID)
	r.PUT("user/:id", Controllers.UpdateUser)
	r.DELETE("user/:id", Controllers.DeleteUser)
return r
}