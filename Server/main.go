package main

import (
	"user-management/Config"
	"user-management/Models"
	"user-management/Routes"
	"fmt"
	"github.com/jinzhu/gorm"
	_"github.com/golang-migrate/migrate"
    _"github.com/golang-migrate/migrate/database/mysql"
	_"database/sql"
)

var err error

func main() {
	Config.DB, err = gorm.Open("mysql", Config.DbURL(Config.BuildDBConfig()))
	if err != nil {
		fmt.Println("Status:", err)
	}
	defer Config.DB.Close()
	Config.DB.AutoMigrate(&Models.User{})
	
	// db, _ := sql.Open("mysql", "root:@tcp(localhost:3306)/usersManagement?multiStatements=true")
    // driver, _ := mysql.WithInstance(db, &mysql.Config{})
    // m, _ := migrate.NewWithDatabaseInstance(
    //     "file:///migrations",
    //     "mysql", 
    //     driver,
    // )
	// m.Steps(2)

	r := Routes.SetupRouter()

	//running
	r.Run(":8089")
}