TES-SV-BRI Server
--------------

Versi Golang = 1.16.6

Database = MySQL

File SQL Database = usersmanagement.sql (import sql)

Database name = usersmanagement

Database username = root

Database password = 

postman collection = Collection User Manag.postman_collection


Run Application
---------------
Run Terminal (pada folder ini) = go run main.go


HOW TO USE
----------
import db di mysql
import postman collection (sudah terdapat body/param untuk diisi)
running program

1.Create User
postman: Input Data User

2.Update User
postman: Change Data User

3.Get User By Id
postman: Get User By Id

4.Delete User
postman: Delete Data User

5.Get All
postman: Get All Data User filtered by limit & offset

