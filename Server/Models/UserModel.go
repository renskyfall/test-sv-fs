//Models/UserModel.go
package Models

type User struct {
	Id     		int    `json:"id" gorm:"primary_key" sql:"AUTO_INCREMENT"`
	Username    *string `json:"username" gorm:"type:varchar(100);unique;not null"`
	Password   	*string `json:"password" gorm:"type:varchar(100)"`
	Name	    string `json:"name" gorm:"type:varchar(100)"`
}

func (b *User) TableName() string {
	return "user"
}