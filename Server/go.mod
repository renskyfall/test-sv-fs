module user-management

go 1.16

require (
	github.com/gin-contrib/cors v1.3.1 // indirect
	github.com/gin-gonic/gin v1.7.4 // indirect
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/golang-migrate/migrate v3.5.4+incompatible
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/hashicorp/go-multierror v1.1.1
	github.com/jinzhu/gorm v1.9.16 // indirect
	github.com/labstack/echo/v4 v4.5.0
	github.com/mattn/go-sqlite3 v2.0.1+incompatible // indirect
	github.com/rs/cors v1.8.0
	go.uber.org/atomic v1.9.0
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5 // indirect
)
