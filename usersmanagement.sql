/*
 Navicat Premium Data Transfer

 Source Server         : renod-mysql
 Source Server Type    : MySQL
 Source Server Version : 100316
 Source Host           : localhost:3306
 Source Schema         : usersmanagement

 Target Server Type    : MySQL
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 09/09/2021 08:47:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (4, 'renrendim', '$2a$14$1EWWXd0O2B.0gCvyH54DUOayV858Fm6tawAdz2orbljBTpg.4s0yO', 'Muhammad Reno Dimas Pradana');
INSERT INTO `user` VALUES (5, 'diamond', '$2a$14$svxFHjBpskWckwIZdQm1Be3hCsma8EHeba1zDxjdb.wF8m010RiYq', 'Muhammad Reno Dimas Pradana');
INSERT INTO `user` VALUES (6, 'renskyfall1909', '$2a$14$da7pHx5irbd8dCUrbC9V1.aKsx86gTRswpFgUcJmBx3IodPTkVtTm', 'Ren Skyfall');
INSERT INTO `user` VALUES (7, 'fikafikasa', '$2a$14$w8gpQdAxfSal3L20NyTPL.gGpygKupc83R2iffdQO29UdjK.bkiCm', 'Fikasa');
INSERT INTO `user` VALUES (8, 'kikiyanagi', '$2a$14$rZrYnlzQ9sSIiT4zUFMyKePB.TfNCMm5PTXSOfppe7suGWIdXoGVq', 'Kiki Andriansyah');
INSERT INTO `user` VALUES (9, 'gijelwangywangy', '$2a$14$.XlwYTRVBaDGjpypEd2q..8wdD2z09gHj9jDTZRFcCtW535UwEWCO', 'Azzri');
INSERT INTO `user` VALUES (10, 'diposutandi', '$2a$14$PRrO/Bp2dLJesWdKLPQyMeGD41KjZkuppqGZ5oDBUvYxSlsPM3sb.', 'Dipo Sutandi');
INSERT INTO `user` VALUES (11, 'gustitaku', '$2a$14$K2x6VQrlEW1jZUjxB43vjuPxt2Ow0MltqVg90i.1CnIP.UlJs46Cq', 'Gusti Rachbini');
INSERT INTO `user` VALUES (12, 'lianreallyhatesren', '$2a$14$0Ny21r98JmvpcDskdO.mZegwR64bbjFi9p2v/fiv/3BFXOR.FXBUu', 'Lian Ho');
INSERT INTO `user` VALUES (13, 'Cherryandina', '$2a$14$poIkLv0B4GOc25a24GtGHeTJgDz7wFpEMFwIfomnwMQoSt788qDle', 'Cherryandina Mutia Kinasih');
INSERT INTO `user` VALUES (14, 'haulanhaulan', '$2a$14$rVcdU30Wo3Ba1s.lE7ByZOBBKROIAFia/i0z9XbbB.m286RYarcRq', 'Haulan Nafi\'ah');
INSERT INTO `user` VALUES (15, 'lemonlemonilo', '$2a$14$x0Xwxh5KiefX7uEWkOpnC.w9br1QALEHXlfqpk5hUb6ToVOcdzOe2', 'Lemonilo rasa Ayam');
INSERT INTO `user` VALUES (36, 'sayalabour', '$2a$14$P9BHhLpigtUS67iiVwCxI.GmZeKd/B.GZ6qVQ2XJQXMvQuIou/noK', 'sayalabour');
INSERT INTO `user` VALUES (37, 'asdasdasd', '$2a$14$daO1wwAIZAT7xNQoFLiuqe4KtrKC8Dbii87/kpl1AxLxmeqnZTf2W', 'asdasdasd');
INSERT INTO `user` VALUES (38, 'xiaoputrajongli', '$2a$14$NK8izJwFDiG0rynNi.z0H.PaLZSmURVy/uYFw8TV1sSz1AAvkNjCS', 'Xiao');
INSERT INTO `user` VALUES (39, 'loganpaul', '$2a$14$EhNh8g1BiIg8HWuuY2bQJ.LF9VHkbcfuvxdMwW2eypZY98xxluvF2', 'Logan Paul');

SET FOREIGN_KEY_CHECKS = 1;
